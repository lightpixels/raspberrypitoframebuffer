#ifndef METHOD_MEMCPY_H
#define METHOD_MEMCPY_H

#include "method_memcpy_global.h"

#include <stdint.h>

METHOD_MEMCPY_SHARED_EXPORT void doubleBufferingCopy(uint16_t* pixelsFrom,
                                                     uint16_t* pixelsTo,
                                                     size_t    nPixels,
                                                     uint16_t* pixelsFront,
                                                     uint16_t* pixelsBack);

#endif // METHOD_MEMCPY_H
