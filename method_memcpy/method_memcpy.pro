QT      -= gui

TARGET   = MethodMemcpy

TEMPLATE = lib

include( ../deploy.pri )
DESTDIR = $$PROJECT_ROOT_DIRECTORY/deploy

DEFINES += METHOD_MEMCPY_LIBRARY

SOURCES += *.c
HEADERS += *.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
