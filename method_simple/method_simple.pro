QT      -= gui

TARGET   = MethodSimple

TEMPLATE = lib

include( ../deploy.pri )
DESTDIR = $$PROJECT_ROOT_DIRECTORY/deploy

DEFINES += METHOD_SIMPLE_LIBRARY

SOURCES += *.c
HEADERS += *.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
