#ifndef METHOD_SIMPLE_H
#define METHOD_SIMPLE_H

#include "method_simple_global.h"

#include <stdint.h>

METHOD_SIMPLE_SHARED_EXPORT void doubleBufferingCopy(uint16_t* pixelsFrom,
                                                     uint16_t* pixelsTo,
                                                     size_t    nPixels,
                                                     uint16_t* pixelsFront,
                                                     uint16_t* pixelsBack);

#endif // METHOD_SIMPLE_H
