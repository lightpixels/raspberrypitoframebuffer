# Double Buffering Memcpy Method #

Implementation of a double buffering copy.

## Usage

Check the repository [RaspberryPi To Framebuffer](https://bitbucket.org/lightpixels/raspberrypitoframebuffer) to setup the benchmark.

## Interface

The point is to copy the data from **uint16_t* pixelsFrom** to **uint16_t* pixelsTo**.

```
#!c

DOUBLEBUFFERINGMEMCPYMETHODSHARED_EXPORT void doubleBufferingCopy(uint16_t* pixelsFrom,
                                                                uint16_t* pixelsTo,
                                                                size_t    nPixels,
                                                                uint16_t* pixelsFront,
                                                                uint16_t* pixelsBack);
```

## Other implementations

* [Double Buffering Copy Method](https://bitbucket.org/lightpixels/doublebufferingcopymethod)