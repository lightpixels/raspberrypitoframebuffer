#ifndef METHOD_MEMCPY_OPENMP_H
#define METHOD_MEMCPY_OPENMP_H

#include "method_memcpy_openmp_global.h"

#include <stdint.h>

METHOD_MEMCPY_OPENMP_SHARED_EXPORT void doubleBufferingCopy(uint16_t* pixelsFrom,
                                                            uint16_t* pixelsTo,
                                                            size_t    nPixels,
                                                            uint16_t* pixelsFront,
                                                            uint16_t* pixelsBack);

#endif // METHOD_MEMCPY_OPENMP_H
