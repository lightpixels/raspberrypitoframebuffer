# RaspberryPi To Framebuffer #

Benchmark for [raspi2fb](https://bitbucket.org/lightpixels/raspi2fb) implementation to speed up the display on PiTFT.

## Initialize ##

```
#!shell

git clone https://jgueytat@bitbucket.org/lightpixels/raspberrypitoframebuffer.git
git submodule --init --remote
cd raspberrypitoframebuffer

```

## Description

Benchmark application:

* [Benchmark](https://bitbucket.org/lightpixels/benchmark)

Libraries compared:

* [DoubleBufferingCopyMethod](https://bitbucket.org/lightpixels/doublebufferingcopymethod)
* [DoubleBufferingMemcpyMethod](https://bitbucket.org/lightpixels/doublebufferingmemcpymethod)

Helper class that fakes the frames:

* [FrameGenerator](https://bitbucket.org/lightpixels/framegenerator)
* [FrameGeneratorTests](https://bitbucket.org/lightpixels/framegeneratortests)

## Usage ##

### In a shell UNIX ###


```
#!shell

mkdir build
cd build
qmake ../
make
cd ../deploy
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:. ./tst_benchmarktest
```

### With Qtcreator ###

1. Open the main *.pro file.

### With Visual Studio ###

1. Double click on QtCreatorToVisualStudio.bat.
2. Open the main *.sln file.