TEMPLATE = subdirs

include(deploy.pri)

SUBDIRS += \
    FrameGenerator \
    method_simple \
    method_memcpy \
    method_simple_openmp \
    method_memcpy_openmp \
    Benchmark \
    FrameGeneratorTests

Benchmark.depends = method_simple method_memcpy method_simple_openmp method_memcpy_openmp FrameGenerator
FrameGeneratorTests.depends = FrameGenerator
