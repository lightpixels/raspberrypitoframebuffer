QT      -= gui

TARGET   = MethodSimpleOpenMP

TEMPLATE = lib

include( ../deploy.pri )
DESTDIR = $$PROJECT_ROOT_DIRECTORY/deploy

DEFINES += METHOD_SIMPLE_OPENMP_LIBRARY

SOURCES += *.c
HEADERS += *.h

QMAKE_CFLAGS += -fopenmp
QMAKE_LFLAGS += -fopenmp

unix {
    target.path = /usr/lib
    INSTALLS += target
}
