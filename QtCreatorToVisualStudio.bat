REM Setting QTDIR, PATH and QMAKESPEC variables!

@set QTDIR=C:\Qt\5.8\msvc2015_64
@set PATH=%QTDIR%\bin;%PATH%
@set QMAKESPEC=win32-msvc2015

REM Generating Visual Studio project from the *.pro files:

@qmake -tp vc -r

REM The project file RaspberryPiToFramebuffer.sln should be created now.

pause
